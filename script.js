
document.getElementById("colPickerButton").onclick = function colorPicker()
{
  //generate 2 random numbers between 1 and 20
  const x = rand(1,20); 
  const y = rand(1,20);

  //while loop to enter random red,green,blue values into array
  let g = 0;
  const COLORS = [] ;
  while (COLORS.length < 21) 
  {
    COLORS[g] = [ rand(0, 255), rand(0, 255), rand(0, 255)];
    g++;
  }
  console.log("2 Random colors selected: ", COLORS[x], COLORS[y]);
  //store the 2 random colors in 2 variables
  const col1 = 'rgb('+COLORS[x]+')';
  const col2 = 'rgb('+COLORS[y]+')';
  console.log("Col1 & Col2: " + col1, col2);

  //set the background of the body to a linear gradient of the 2 random colors
  document.getElementById("body").style.background= "linear-gradient(to right, "  
    + col1 + ", "  
    + col2 + ")" ;

  //get value of foreground color
  const txtColor1 = $("body").css("color").valueOf();
  console.log("txtColor1: " + txtColor1);
  //get the red,green,blue values of the foreground color
  colorsOnly = txtColor1.substring(txtColor1.indexOf('(') + 1, txtColor1.lastIndexOf(')')).split(/,\s*/);
  var components = 
  {
    red: colorsOnly[0],
    green: colorsOnly[1],
    blue: colorsOnly[2]

  };
  //check the brightness of the foreground color
  const colorBrightnessForeground = testColorBrightness(components);
  console.log("Foreround Brightness: " + colorBrightnessForeground);
  //check the brightness of the background color
  const backgroundColor1 = 
  {
    red: COLORS[x][0],
    green: COLORS[x][1],
    blue: COLORS[x][2]
  }
  const backgroundColor2 = 
  {
    red: COLORS[y][0],
    green: COLORS[y][1],
    blue: COLORS[y][2]
  }

  //get the darker background color after comparison
  const backCol1 =testColorBrightness(backgroundColor1);
  const backCol2 =testColorBrightness(backgroundColor2);
  const BackgroundBrightness = ((backCol1)+ (backCol2));
  //calculate the brightness difference between avg background brightness and foreground brightness
  const colorBrightnessDifference = ((BackgroundBrightness) - (colorBrightnessForeground));
  console.log("BrightnessDifference: " + colorBrightnessDifference);
  
  //calculate the color difference between foreground and background
  const colorDiff = testColorDifference(components,backgroundColor1);
  console.log("color Difference: " + colorDiff);
  

    //check if brightness difference is = to 125 or greater
    if(colorBrightnessDifference <125)
    {
      console.log("brightness difference Less than 125");
      //we change the foreground color
      let colOnly1 = Number(COLORS[x][0]);
      let colOnly2 = Number(COLORS[x][1]);
      let colOnly3 = Number(COLORS[x][2]);

      colOnly1 = (colOnly1) + rand(0,100);
      colOnly2 = (colOnly2) + rand(0,100);
      colOnly3 = (colOnly3) + rand(0,100);

      const finalColor = 'rgb('+colOnly1+','+colOnly2+','+colOnly3+')'
      console.log("FinalColor: "+ finalColor);
  
      //using jquery to set the background to the new color
      $(document).ready(function(){ 
      $("body").css("background", ("linear-gradient(to right, "+ finalColor + ", " + col2 + ")"));
      
      console.log($("body").css("color").valueOf(finalColor));
      });
      const newFinalColor = icolor(finalColor);
      console.log(newFinalColor);
      const finalColorBrightness = testColorBrightness(newFinalColor);
      console.log("New brightness: " + ((finalColorBrightness) + (backCol2)));
      const finalColorBrightnessDifference = ((finalColorBrightness) + (backCol2)) - (colorBrightnessForeground);
      console.log("New brightness Difference: " + finalColorBrightnessDifference)
    }
    else
    {
      console.log("brightness difference is greater than 125");
    }

}
function icolor(rgb){
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  const hello = 
  {
    red: parseInt(rgb[1]),
    green:parseInt(rgb[2]),
    blue: parseInt(rgb[3]),
  }
  return hello;

 }

//function to return random number from a range of 2 numbers
function rand(frm, to) {
    return (Math.floor (Math.random() * (to - frm))) + frm;
}

//function to test the brightness of a color - converting the RGB values to YIQ values
  function testColorBrightness(color)
  {
   return colorBrightness1 =  ((color.red * 299) + (color.green * 587) + (color.blue * 114)) / 1000
  }
  function testColorDifference(colorA, colorB)
  {
    const colorDif =  (Math.max(colorA.red , colorB.red ) - Math.min (colorA.red , colorB.red)) 
  + (Math.max (colorA.green , colorB.green) - Math.min (colorA.green , colorB.green)) 
  + (Math.max(colorA.blue , colorB.blue) - Math.min(colorA.blue , colorB.blue))
    
    return colorDif;
  }
